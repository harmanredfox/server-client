﻿using System;
using System.Text;
using Java.Net;
using System.Threading.Tasks;

namespace SimpleClient
{
    public class AsynchronousClient : IDisposable
    {
        Socket client;

        public AsynchronousClient() { }

        public async Task<bool> ConnectToServer(byte[] ipAddress, int port)
        {
            var address = InetAddress.GetByAddress(ipAddress);

            client = new Socket();
            await client.ConnectAsync(new InetSocketAddress(address, port));

            return client.IsConnected;
        }

        public async void Send(string message)
        {
            if (client != null && client.IsConnected)
            {
                var buffer = Encoding.ASCII.GetBytes(message);
                await client.OutputStream.WriteAsync(buffer, 0, buffer.Length);
            }
        }

        public void Disconnect()
        {
            if (client != null && client.IsConnected)
            {
                var endMessage = Encoding.ASCII.GetBytes("<EOF>");

                client.OutputStream.Write(endMessage, 0, endMessage.Length);
                client.Close();
                client.Dispose();
                client = null;
            }
        }

        public void Dispose()
        {
            Disconnect();
        }
    }
}