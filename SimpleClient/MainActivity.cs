﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System;
using System.Text;

namespace SimpleClient
{
    [Activity(Label = "SimpleClient", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private AsynchronousClient client = null;

        private EditText ipAddressEditText, portEditText, messageEditText;
        private Button connectBtn, disconnectBtn, sendMessageBtn;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            ipAddressEditText = FindViewById<EditText>(Resource.Id.IPAddressEditText);
            portEditText = FindViewById<EditText>(Resource.Id.PortEditText);
            messageEditText = FindViewById<EditText>(Resource.Id.MessageEditText);

            connectBtn = FindViewById<Button>(Resource.Id.ConnectButton);
            disconnectBtn = FindViewById<Button>(Resource.Id.DisconnectButton);
            sendMessageBtn = FindViewById<Button>(Resource.Id.SendButton);

            SetUIState(false);

            connectBtn.Click += async (sender, e) =>
            {
                client = new AsynchronousClient();

                var ip = ipAddressEditText.Text.Split('.')
                    .Select(a => byte.Parse(a)).ToArray();

                var connected = await client.ConnectToServer(ip,
                    int.Parse(portEditText.Text));

                SetUIState(connected);
            };

            disconnectBtn.Click += (sender, e) =>
            {
                client?.Disconnect();

                SetUIState(false);
            };

            sendMessageBtn.Click += (sender, e) =>
            {
                client?.Send(messageEditText.Text);
                messageEditText.Text = string.Empty;
            };
        }

        protected override void OnDestroy()
        {
            client?.Dispose();
            client = null;
            base.OnDestroy();
        }

        private void SetUIState(bool connected)
        {
            connectBtn.Enabled = !connected;
            ipAddressEditText.Enabled = !connected;
            portEditText.Enabled = !connected;
            messageEditText.Enabled = connected;
            disconnectBtn.Enabled = connected;
            sendMessageBtn.Enabled = connected;
        }
    }
}

