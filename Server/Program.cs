﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Net.NetworkInformation;

using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace Server
{
    class Program
    {
        private static ScriptEngine engine;

        static void Main(string[] args)
        {
            engine = Python.CreateEngine();

            // Get IP address of Wi-Fi connection
            var addresses = NetworkInterface.GetAllNetworkInterfaces()
                .Where(i => i.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                .SelectMany(i => i.GetIPProperties().UnicastAddresses)
                .Where(a => a.Address.AddressFamily == AddressFamily.InterNetwork)
                .Select(a => a.Address)
                .ToList();

            AsynchronousServer server = null;

            if (addresses.Any())
            {
                var ipAddress = addresses.First();
                server = new AsynchronousServer(ipAddress);

                server.DataReceived += Server_DataReceived;
                server.Disconnected += Server_ServerDisconnected;

                var port = server.StartListening();
                Console.WriteLine($"Server has been started: {ipAddress.ToString()}:{port}");
                Console.WriteLine("Waiting for a connection...");
            }
            else
            {
                Console.WriteLine("Connect to Wi-Fi network!");
            }

            Console.WriteLine("Press any key to disconnect server...");
            Console.Read();

            server?.StopListening();
        }

        private static void Server_ServerDisconnected()
        {
            Console.WriteLine("Server disconnected!");
        }

        private static void Server_DataReceived(string content, int readBytes)
        {
            var lowerCaseContent = content.ToLower();

            if ("up".Equals(lowerCaseContent) ||
                "down".Equals(lowerCaseContent) && engine != null)
            {
                engine.Execute($"print \"{content}\"");
            }
            else
            {
                Console.WriteLine($"Readed {readBytes} bytes\n\tContent: {content}");
            }
        }
    }
}
