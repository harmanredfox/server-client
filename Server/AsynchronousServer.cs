﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class AsynchronousServer
    {
        private IPAddress ipAddress;
        private int port;
        private bool isAlive = false;
        private StateObject state;

        public delegate void DataReceivedHandler(string content, int readBytes);
        public delegate void ServerDisconnectedHandler();

        public event DataReceivedHandler DataReceived;
        public event ServerDisconnectedHandler Disconnected;

        public AsynchronousServer(params byte[] ipAddress)
        {
            if (ipAddress.Length != 4)
            {
                throw new ArgumentOutOfRangeException("Wrong IP Address");
            }
            else
            {
                this.ipAddress = new IPAddress(ipAddress);
            }
        }

        public AsynchronousServer(IPAddress ipAddress)
        {
            this.ipAddress = ipAddress;
        }

        public int StartListening()
        {
            isAlive = true;
            port = new Random().Next(45000, 46000);

            var localEndPoint = new IPEndPoint(ipAddress, port);
            var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            listener.Bind(localEndPoint);
            listener.Listen(1);

            listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

            return port;
        }

        public void StopListening()
        {
            if (isAlive)
            {
                isAlive = false;
                state?.workSocket?.BeginDisconnect(false, new AsyncCallback(DisconnectCallback), state);
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            state = new StateObject
            {
                workSocket = (ar.AsyncState as Socket).EndAccept(ar)
            };

            while (isAlive)
            {
                var bytesRead = state.workSocket.Receive(state.buffer);

                if (bytesRead > 0)
                {
                    var content = Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead);

                    if (content.Contains("<EOF>"))
                    {
                        StopListening();
                    }

                    DataReceived?.Invoke(content, bytesRead);
                }
            }
        }

        private void DisconnectCallback(IAsyncResult ar)
        {
            var state = ar.AsyncState as StateObject;
            state.workSocket.EndDisconnect(ar);

            state.workSocket.Dispose();
            state = null;

            Disconnected?.Invoke();
        }

        internal class StateObject
        {
            // Client  socket.
            public Socket workSocket = null;
            // Size of receive buffer.
            public const int BufferSize = 1024;
            // Receive buffer.
            public byte[] buffer = new byte[BufferSize];
            // Received data string.
            public StringBuilder sb = new StringBuilder();
        }
    }
}
