import asyncio
import time
import argparse
import subprocess
import re
import binascii
from os import environ
from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner
from autobahn.wamp.serializer import MsgPackSerializer


parser = argparse.ArgumentParser(prog='Podium Demo Examples')
parser.add_argument('--adjust-volume', default=False, type=int, help='adjust music volume (-100..100)')
parser.add_argument('--pause', action='store_true', help='play/pause current playback')
parser.add_argument('--ip', metavar='IP', help='ip of device')
args = parser.parse_args()

DEFAULT_SERVICE_IP="172.20.20.20"
SERVICE_IP=args.ip

def wakeup():
    # Call again
    loop.call_later(0.1, wakeup)

def ping(host):
    """
    Returns True if host responds to a ping request
    """
    import os, platform

    # Ping parameters as function of OS
    ping_cmd = ['ping']
    check_function = None
    if platform.system().lower() == "windows":
        ping_cmd += ['-n', '1', '-w', '50']
        check_function = subprocess.check_output
    else:
        ping_cmd += ['-c', '1']
        check_function = subprocess.check_call
    ping_cmd.append(host)
    # Ping
    try:
        check_function(ping_cmd)
    except Exception as e:
        return False
    return True

class Component(ApplicationSession):
    """
    An application component calling the different backend procedures.
    """

    async def onJoin(self, details):

        try:
            if args.pause:
                #get music status
                res = await self.call(u'com.harman.stateGet')
                print("volumeAdjust: {}".format(res.kwresults['music']))
                #set state for music
                if res.kwresults['music']['state'] == 'playing' :
                    res = await self.call(u'com.harman.music.pause')
                elif res.kwresults['music']['state'] == '' :
                    res = await self.call(u'com.harman.music.resume')
                else :
                    print('Nothing to do')
                #print("volumeAdjust: {}".format(res))
                
            if args.adjust_volume:
                res = await self.call(u'com.harman.volumeAdjust', args.adjust_volume)
                print("volumeAdjust: {}".format(res))

        except Exception as e:
            print('Error: "{}"'.format(e))

        self.leave()

    def onDisconnect(self):
        asyncio.get_event_loop().stop()


if __name__ == '__main__':

    print("")
    while ping(SERVICE_IP) == False:
        print("Ping: No ping response from {} .. trying again ..".format(SERVICE_IP))
    print("Ping: Reply from {}".format(SERVICE_IP))
    print("")

    msgpack = MsgPackSerializer()
    runner = ApplicationRunner(
        url=environ.get("PODIUM_DEVICE_ADDR", u"ws://{}:9998/ws".format(SERVICE_IP)),
        realm=u"default",
		serializers=[msgpack],
    )
    try:
        runner.run(Component)
    except Exception as e:
        print('Error: "{}"'.format(e))
