from os import environ
import socket
import argparse

parser = argparse.ArgumentParser(prog='Notebook api demo examples')
parser.add_argument('--slide', default=False, help='sliding')
parser.add_argument('--ip', metavar='IP', help='ip of device')
parser.add_argument('--port', type=int, help='port of device')
args = parser.parse_args()

DEFAULT_SERVICE_IP="172.20.20.20"
SERVICE_IP=args.ip
SERVICE_PORT=args.port

def ping(host):
    """
    Returns True if host responds to a ping request
    """
    import os, platform

    # Ping parameters as function of OS
    ping_cmd = ['ping']
    check_function = None
    if platform.system().lower() == "windows":
        ping_cmd += ['-n', '1', '-w', '50']
        check_function = subprocess.check_output
    else:
        ping_cmd += ['-c', '1']
        check_function = subprocess.check_call
    ping_cmd.append(host)
    # Ping
    try:
        check_function(ping_cmd)
    except Exception as e:
        return False
    return True


if __name__ == '__main__':

    if args.slide :
        print('slide ' + args.slide)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((SERVICE_IP, SERVICE_PORT))
        sending_str = "slide " + args.slide
        s.send(sending_str.encode('utf-8'))
        s.close()
