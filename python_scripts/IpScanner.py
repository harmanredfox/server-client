import time
import subprocess
import re

class IpScanner :
    list_of_ips = {}
    last_updated = 0
    update_interval = 30

    @staticmethod
    def get_ip_by_mac( mac ) :
        print('last : ' + str(IpScanner.last_updated) + ' current : ' + str(time.time()))
        if (time.time() - IpScanner.last_updated) > IpScanner.update_interval :
            print('scan by arp')
            IpScanner.scan_by_arp()
        if mac in IpScanner.list_of_ips.keys() :
            return IpScanner.list_of_ips[mac]
        else :
            return ''

    @staticmethod
    def scan_by_arp() :
        process = subprocess.Popen('arp-scan --interface=wlan0 --localnet', shell=True, stdout=subprocess.PIPE)
        output,error = process.communicate()
        for line in output.split('\n') :
            ip = re.search(r'[0-9]+(?:\.[0-9]+){3}', line)
            mac = re.search(r'[0-9a-f]+(?::[0-9a-f]+){5}', line)
            if ip and mac :
                IpScanner.last_updated = time.time()
                IpScanner.list_of_ips[mac.group(0)] = ip.group(0)
                
                
        
