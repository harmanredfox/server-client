import json
from device import *

class DataProcessor:
    list_of_actions = []
    list_of_devices = {}

    def __init__(self):
        with open('skills.dat') as f:
            self.list_of_actions = f.read().splitlines()
        json_devices_data = json.load(open('devices.json'))
        for json_device in json_devices_data:
            added_device = Device(json_device)
            self.list_of_devices[ json_device['name'] ] = added_device

    def get_intersected_device( x, y, z, angle) :
        #TODO
        print("get_intersected_device")

    def process_data(self, raw_data ):
        #get body
        parsed_data = json.loads(raw_data)
        print(parsed_data)
        action = parsed_data["action"]
        if action in self.list_of_actions :
            print('Action : ' + action)
            #TODO get sensors data
            #TODO detect thing which received signal
            #TODO send signal for thing

            #WARNING temporarily
            if action in [ 'move_up', 'move_down', 'special_skill' ] :
                #call cortana skill
                print("call cortana skill")
                if not self.list_of_devices['invoke'].execute(action) :
                    print('Device not responde')
            if action in [ 'move_left', 'move_right' ] :
                #call presentation skill
                print("call presentation skill")
                self.list_of_devices['notebook'].execute(action)
