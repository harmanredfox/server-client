from bluetooth import *
from dataprocessor import *

def configure_server( server ) :
    port = PORT_ANY
    server.bind(("",port))
    server.listen(1)

bt_server = BluetoothSocket( RFCOMM )
configure_server(bt_server)
# The service UUID to advertise
uuid = "7be1fcb3-5776-42fb-91fd-2ee7b5bbb86d"
 
# Start advertising the service
advertise_service(bt_server, "RaspiBtSrv",
                   service_id=uuid,
                   service_classes=[uuid, SERIAL_PORT_CLASS],
                   profiles=[SERIAL_PORT_PROFILE])
                   
data_proc = DataProcessor()
#TODO test
data_proc.process_data('{\"action\" : \"move_right\"}')

while True:
    #wait for connection
    client_sock,address = bt_server.accept()
    print "Accepted connection from ",address

    try :
        #wait for message
        while True:
            data = client_sock.recv(1024)
            if not data: break
            #process message
            print "received [%s]" % data
            data_proc.process_data(data)
    except:
        print('exception')

    #disconnect and wait for new connection
    client_sock.close()
	
	
bt_server.close()
