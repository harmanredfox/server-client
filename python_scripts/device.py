import json
import subprocess
import re
from IpScanner import *


class Device :
    coordinate = [ 0, 0 ]
    ip = '172.20.20.20'
    mac = ''
    status = 'offline'
    name = 'unnamed'
    api_file = ""

    def __init__(self, json_obj):
        self.name = json_obj['name']
        self.coordinate = json_obj['coordinate']
        self.mac = json_obj['mac']
        self.api_file = json_obj['api_file']
        #load skills
        self.skills = {}
        skills_json = json.load(open(json_obj["skills_file"]))
        for skill in skills_json:
            self.skills[ skill['binded'] ] = skill['cmd']
            
        #TODO make a background check
        #get ip by mac in localnet
        self.ip = IpScanner.get_ip_by_mac(self.mac)
        self.status = 'online' if self.ip else 'offline'
        
        print(self.ip + ' ' + self.status)
                        
        print (self.name + ' ' + str(self.coordinate) + ' ' + self.mac + ' ' + self.ip + ' ' + str(self.skills))

    def check_intersection(self, coordinate, angle ) :
        if self.status == 'offline' :
            return false
        #TODO

    def execute(self, action ) :
        if self.status == 'offline' :
            return False
        print('call python3 ' + self.api_file + ' --ip ' + self.ip + ' ' + self.skills[action])
        command = 'python3 ' + self.api_file + ' --ip ' + self.ip + ' ' + self.skills[action]
        subprocess.Popen(command, shell=True)
        #TODO check output. If disconnected - update ip and status
        
